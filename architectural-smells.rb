#!/bin/ruby
require 'csv'

class String
	def last_index_of (char)
		self.size - self.reverse.index(char) - 1
	end
end

module AS

	# Rimuove tutti/e i/le package/classi che soddisfano almeno un elemento delle seguenti espressioni regolari
	# Fare molte attenzione alle modifiche a questa costante
	ENTITY_FILTER = [/^(org\.eclipse).*$/, /^(org\.xml).*$/, /^((\[L)?java).*$/, /^(org\.w3c).*$/, /^(org\.ietf).*$/]

	class ArchitecturalSmell
		def arc_smell_uuid
			
		end

	end

	# File non filtrati di Arcan
	class UnfilteredCyclicDependency
		attr_accessor :package, :n_cycles
		
		def initialize(package, n_cycles)
			@package, @n_cycles = package, n_cycles
		end

		def self.parse_csv(file_path)
			all_rows = CSV.read(file_path)
			pckgs_list = all_rows.shift # Estrai la lista di package
			pckgs_list.shift # Rimuove intestazione colonna cicli
			cycle_count = Array.new(pckgs_list.size) { |i| i = 0 }
			all_rows.each do |row|
				row.shift	# Rimuove il numero del ciclo
				row.each_with_index do |p, i|
					cycle_count[i] = cycle_count[i] + p.to_i
				end
			end
			cyclic_list = []
			pckgs_list.zip(cycle_count) do |p, c|
				cyclic_list << UnfilteredCyclicDependency.new(p, c)
			end
			return cyclic_list
		end
		
		def to_s
			format("Package: %s cycles: %i", @package, @n_cycles)
		end
	end

	class CyclicDependency < ArchitecturalSmell
		attr_accessor :id_cycle, :type, :members

		MIN_JACCARD_SIMILARITY = 0.85

		def initialize(id_cycle, type, members)	
			@id_cycle, @type, @members = id_cycle, type, members
		end

		# Parsa un file csv contenente i risultati di ARCAN contenenti Cyclic Dependency
		def self.parse_csv(file_path)
			cyclic_list = []
			CSV.foreach(file_path, {:headers => true}) do |row|
				ignore = false;
				cycle_members = row['ElementList'].split(',')
				ENTITY_FILTER.each do |f|
					ignore = ignore || !cycle_members.index {|x| f =~ x}.nil?
				end
				next if ignore
				cyclic_list << CyclicDependency.new(row['IdCycle'], row['CycleType'], cycle_members)
			end
			return cyclic_list
		end

		# Un AS Cyclic è uguale ad un altro se è dello stesso tipo e affligge gli stessi package
		def ==(other)
			return false if other == nil || self.class != other.class
			isEqual = other.type == @type
			jaccard = @members.jaccard_index(other.members)
			max = @members.size > other.members.size ? @members : other.members
			min = @members.size <= other.members.size ? @members : other.members
			isEqual = (isEqual && (jaccard >= MIN_JACCARD_SIMILARITY)) # (jaccard - min.size.to_f / max.size.to_f).abs <= 0.1
			if false # Debug 
				puts "this type: #{@type}\tother type: #{other.type}"	
				puts "max: #{max.sort}\nmin: #{min.sort}\nj: #{jaccard}"
				puts "s_ration #{min.size.to_f / max.size.to_f}"
				puts "equal: #{isEqual}"
				puts "*"*20
			end
			return isEqual
		end

		def eql?(other)
			self == other
		end

		def hash
			@type.hash
		end

		def to_s
			format("Type: %-10s Members: %s", @type, @members.inspect)			
		end

		# Override per generare un uuid unico e sempre identico
		def arc_smell_uuid
			"CD_#{@type}_#{members.sort.hash}" 
		end
	end

	class Hublike < ArchitecturalSmell
		attr_accessor :tclass, :fanin, :fanout, :total
		def initialize(tclass, fanin, fanout, total)
			@tclass, @fanin, @fanout, @total = tclass, fanin, fanout, total
		end

		# Parsa un file csv ritornando una lista di oggetti Hublike
		# Ignora tutti gli hublike che matchano almeno un ENTITY_FILTER
		def self.parse_csv(file_path)
			hublike_list = []
			CSV.foreach(file_path, {:headers => true}) do |row|
				ignore = false
				ENTITY_FILTER.each do |f|
					ignore = ignore || f =~ row["Class"]
				end
				next if ignore
				hublike_list << Hublike.new(row["Class"], row["FanIn"], row["FanOut"], row["Total Dependences"])
			end
			return hublike_list
		end

		# Due HL sono uguali se si riferiscono alla stessa classe
		def ==(other)
			return false if other == nil
			return @tclass == other.tclass
		end

		def eql?(other)
			self == other
		end

		def hash
			@tclass.hash			
		end

		def to_s
			format("%s, %s, %s, %s", @tclass, @fanin, @fanout, @total)
		end

		# Override per generazione uuid unico
		def arc_smell_uuid
			"HL_#{@tclass}_#{@tclass.hash}"
		end
	end

	class UnstableDependency < ArchitecturalSmell
		attr_accessor :package, :package_instability
		attr_reader :correlated_packages

		def initialize(package, package_instability)
			@package = package
			@package_instability = package_instability
			@correlated_packages = Hash.new
		end

		# Aggiunge una dipendenza al package rappresentato da questa istanza
		# Params:
		# +package+:: Stringa contenente il nome del package
		# +instability+:: La misura dell'instabilità (dato numerico a virgola mobile)
		def add_dependency(package, instability)
			@correlated_packages[package] = instability
		end

		# Rimuove il package fornito in input dalle dipendenze 
		# Params:
		# +package+:: Stringa contenente il nome del package
		def remove_dependency(package)
			@correlated_packages.delete(package)
		end

		def include_dependency?(package)
			@correlated_packages.include? package
		end

		# Due UD sono uguali se si riferiscono allo stesso package, e sono correlati agli stessi package
		def ==(other)
			return false if other == nil || self.class != other.class
			isEqual = @package == other.package 
			isEqual = (isEqual && other.correlated_packages.keys.uniq.sort == @correlated_packages.keys.uniq.sort)
			return isEqual
		end

		def eql?(other)
			self == other
		end

		# Teoricamente sarebbe meglio togliere l'hash dei package correlati, ma non si sa mai....
		def hash
			@package.hash + @correlated_packages.keys.uniq.sort.hash
		end

		# Parsa il file CSV e ritorna una lista di oggetti UnstableDependency.
		# Ignora tutte le dependency che appartengono all'entity filter
		# Params:
		# +file_path+:: Il percorso del file csv da cui estrarre i dati
		def self.parse_csv(file_path)
			unstable_set = []
			CSV.foreach(file_path, {:headers => true}) do |row|
				ignore = false
				ENTITY_FILTER.each do |f|
					ignore = ignore || f =~ row["UnstableDependenciesPackage"]
				end
				next if ignore
				unst_dep = UnstableDependency.new(row["UnstableDependenciesPackage"], row["InstabilityUnstableDependenciesPackage"])
				indx = unstable_set.index{|e| unst_dep.package == e.package}
				# if unstable_set.include?(unst_dep) # usa ==, la sua definizione è cambiata a livello concettuale
				# indx = unstable_set.index(unst_dep)
				if indx != nil
					unstable_set[indx].add_dependency(row["CorrelatedPackage"], row["InstabilityCorrelatedPackage"])
				else
					unst_dep.add_dependency(row["CorrelatedPackage"], row["InstabilityCorrelatedPackage"])
					unstable_set << unst_dep
				end
			end
			return unstable_set
		end

		def to_s
			format("Package: %s, instability: %f, dependences: %s", @package, @package_instability, @correlated_packages.inspect)
		end

		# Override per generazione uuid unico
		def arc_smell_uuid
			"UN_#{@package}_#{correlated_packages.keys.sort.hash}"
		end
	end

	# Parsa i risultati dei file e li ritorna ordinati per versione e per package
	# Ritorna un hash che come chiavi utilizza le versioni di apache e come valori
	# altri hash, uno per ogni smell accessibili con :hublike, :cyclic, :unstable
	# Esempio: result['7.0.2'][:unstable]['nome.package'] -> n° di unstable dependency nella versione 7.0.2 nel package 'nome.package'
	# Params:
	# +apache_versions_dir+:: Cartella contenente tutte le versioni di tomcat
	def AS.smells_per_package(apache_versions_dir)
		smells = AS.parse_results_files apache_versions_dir		
		#version = '6.0.1'
		smells_per_package = Hash.new
		smells.keys.each do |version|
			packages = AS.parse_entities_list(apache_versions_dir, version)
			
			unstable_per_package 	= Hash.new 0	#
			cyclic_per_package		= Hash.new 0 	# Default value = 0
			hublike_per_package		= Hash.new 0	#
			
			packages.each do |package|
				cycles = smells[version][:cyclic].select{|cd| cd.members.include? package}
				cyclic_per_package[package] += cycles.size unless cycles.nil?
				
				unst_dep = smells[version][:unstable].select{|ud| ud.package == package}.first
				unstable_per_package[package] += unst_dep.correlated_packages.size unless unst_dep.nil?

				hublike = smells[version][:hublike].select do |hl| 
					className = hl.tclass[(hl.tclass.last_index_of('.')+1)..hl.tclass.size]
					package + "." + className == hl.tclass
				end
				hublike_per_package[package] += hublike.size unless hublike.nil?
			end
			smells_per_package[version] = {:hublike => hublike_per_package, :unstable => unstable_per_package, :cyclic => cyclic_per_package}
		end
		return smells_per_package
	end

	# Salva un file csv contenente in numero di AS rilevati in ciascuna versione ed in ciascun package
	def AS.save_smells_per_package(apache_versions_dir, out_file)
		smells_package = AS.smells_per_package(apache_versions_dir)
		CSV.open(out_file, "wb") do |csv|
			csv << ["Version", "Package",  "Cyclic Dep. count", "Unstable Dep. count", "Hublike count"]
			smells_package.keys.each do |version|
				#puts smells_package[version][:hublike], smells_package[version], smells_package[version][:cyclic][package]
				smells_package[version][:hublike].keys.each do |package|
					hl_count = smells_package[version][:hublike][package]
					ud_count = smells_package[version][:unstable][package]
					cd_count = smells_package[version][:cyclic][package]
					csv << [version, package, cd_count, ud_count, hl_count]
				end			
			end
		end
		puts "File successfully written"
	end

	# Parsa i risultati di arcan presenti nella cartella fornita in input
	# Ritorna un hash le cui chiavi sono le versioni di apache tomcat e i valori
	# sono a loro volta degli hash le cui chiavi sono: :hublike, :cyclic, :unstable
	# Params:
	# +apache_versions_dir+:: Cartella contenente tutte le versioni di tomcat
	def AS.parse_results_files(apache_versions_dir)
		dirs = Dir.entries(apache_versions_dir).select { |f| f[/apache-tomcat/]}
		arch_smells = Hash.new
		#dirs = ["apache-tomcat-7.0.10"]
		dirs.each do |apache_dir|
			version = apache_dir[/(\d+\.\d+\.\d+(\.M\d*)?)/]
			results_dir = "#{apache_versions_dir}/#{apache_dir}/results/"
			hublike 	= Hublike.parse_csv(results_dir+"HL.csv")
			unst_dep 	= UnstableDependency.parse_csv(results_dir+"UD.csv")
			cyclic_dep 	= CyclicDependency.parse_csv(results_dir+"packageCL.csv")
			arch_smells[version] = {:hublike => hublike, :unstable => unst_dep, :cyclic => cyclic_dep}
		end
		arch_smells.sort{|x, y| CVE.compare_version(x.first, y.first)}.to_h
	end

	# Parsa la lista del tipo di entità indicato relativo alla versione di apache tomcat indicata
	# Il tipo di entità di default sono i package.
	# Le entità che soddisfano almeno un'espressione regolare presente in ENTITY_FILTER vengono rimosse
	# Params:
	# +apache_versions_dir+:: Cartella contenente tutte le versioni di tomcat
	# +version+:: Versione di apache desiderata. Deve rispettare /(\d+\.\d+\.\d+(\.M\d*)?)/
	# +entity+:: Indica il tipo di entità di cui ottenere la lista. I valori possibili sono 'package' o 'class'
	def AS.parse_entities_list(apache_versions_dir, version, entity = "package")
		dir = Dir.entries(apache_versions_dir).select {|f| f[/(\d+\.\d+\.\d+(\.M\d*)?)/] == version}
		dir = "#{apache_versions_dir}/#{dir.first}/lib/ArcanOutput/#{entity}CyclicDependencyMatrix.csv"
		entities = []
		CSV.foreach(dir, {:headers => true}) do |row|
			entities << row["Cycle"]
		end
		entities.reject! {|e|
			delete = false
			ENTITY_FILTER.each {|x| delete = true if e.match(x)}
			delete
		}
	end

	# Stampa a schermo tutti gli smell parsati, utile per scopi di debug
	def AS.print_all_smells
		AS.parse_results_files('apache-tomcat').each do |k, v|
			puts "Version: #{k}"
			puts "Hublike smells: #{v[:hublike].size}"
			v[:hublike].each do |e|
				puts e
			end
			puts "\nUnstable dependency smells: #{v[:unstable].size}"
			v[:unstable].each do |e|
				puts e
			end
			puts "\nCyclic dependency smells: #{v[:cyclic].size}"
			v[:cyclic].each do |e|
				puts e
			end
		end
	end
end

=begin
AS::UnstableDependency.parse_csv('/home/fenn/Documenti/git/progetto-reverse/apache-tomcat/apache-tomcat-6.0.0/lib/ArcanOutput/UD.csv').each do |e|
	puts "\n#{e}\n"
end
AS::UnfilteredCyclicDependency.parse_csv('/home/fenn/Documenti/git/progetto-reverse/apache-tomcat/apache-tomcat-6.0.0/lib/ArcanOutput/packageUnfilteredCyclicDependencyTable.csv').each {|c| print "#{c.n_cycles} "}
=end