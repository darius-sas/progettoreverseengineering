#!/bin/ruby

require	'./data-extraction-scripts'
require './architectural-smells'
require './common-vulnerabilities'
require	'./dataset-generation'
require 'ripl'
require 'csv'

module Project
	def Project.save_cve_and_as(apache_versions_dir, out_file)
		arc_smells = AS.parse_results_files(apache_versions_dir)
		cve_list = CVE.get_versions_cve_list(apache_versions_dir)
		CSV.open(out_file, "wb") do |csv|
			csv << ["Version", "CVE count", "Unstable Dep. Count", "Cyclic Dep. Count", "Hublike Count"]
			arc_smells.each do |version, a_smells| 
				csv << [version, cve_list[version].count, a_smells[:unstable].size, a_smells[:cyclic].size, a_smells[:hublike].size]
			end
		end
		puts "File #{out_file} written successfully."
	end
end

dataset = DatasetGenerator::load_file_dump('data_final/data.dump')
puts "Dataset loaded from dump file."

Ripl.start