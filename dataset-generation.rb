require	'./data-extraction-scripts'
require './architectural-smells'
require './common-vulnerabilities'

class DatasetGenerator
	attr_accessor :cve_list, :cve_hash, :as_hash, :as_package_hash
	attr_accessor :apache_dir

	def initialize(cve_list, cve_hash, as_hash, as_package_hash)
		@cve_list = cve_list 				# Lista di tutte le cve
		@cve_hash = cve_hash 				# Hash of cve per version cve_hash[version] -> CVE list for this version
		@as_hash = as_hash 					# Hash of as per version  as_hash[version][:hublike] -> list of smells
		@as_package_hash = as_package_hash  # Hash of as per version and package hash[version][:hublike][package]	
	end

	def initialize(apache_versions_dir)
		@cve_list = CVE.get_cve_list
		@cve_hash = CVE.get_versions_cve_list(apache_versions_dir)
		@as_hash = AS.parse_results_files(apache_versions_dir)
		@as_package_hash = AS.smells_per_package(apache_versions_dir)
		@apache_dir = apache_versions_dir
	end

	# Ritorna un oggetto DatasetGenerator a partire da un file di Dump precedentemente generato con #dump_to_file.
	# +filePath+:: Percorso del file di dump contenente l'oggetto da caricare.
	def self.load_file_dump(filePath)
		File.open(filePath, "rb")do |f|
			Marshal::load(f.read)
		end
	end

	def self.update_dump_file(apache_dir, out_file)
		daataset = DatasetGenerator.new apache_dir
		daataset.dump_to_file(out_file)
	end

	# Salva l'oggetto corrente sul file indicato come dump.
	# Params:
	# +filePath+:: Percorso del file su cui salvare il dump.
	def dump_to_file(filePath)
		serialized_object = Marshal::dump(self)
		File.open(filePath, "wb")do |f|
			f << serialized_object
		end
	end

	# Ritorna una matrice dove le colonne sono le cve e le righe sono le versioni.
	# L'elemento (i, j) contiene 1 o 0 se la versione i è affetta dalla vulnerabilità j o meno.
	def binary_CVE_matrix
		versions = Generic.get_all_versions @apache_dir
		matrix = []
		versions.each do |v|
			line = []
			@cve_list.each do |cve|
				line << (cve.affects?(v) ? 1 : 0)
			end
			matrix << line
		end
		matrix
	end

	def binary_matrices_to_csv(out_dir)
		versions = Generic.get_all_versions @apache_dir
		cve_matrix = binary_CVE_matrix
		cve_col_header = @cve_list.collect{|cve| cve.cve_id}
		cve_file = out_dir+"/cve_matrix.csv"
		save_binary_matrix_to_csv(cve_matrix, cve_file, cve_col_header, versions)
		puts "File #{cve_file} successfully written."

		as_matrices, as_headers = binary_AS_matrices
		as_matrices.each do |k, v| 
			as_file = out_dir + "/#{k}_matrix.csv"
			col_header = as_headers[k]
			save_binary_matrix_to_csv(v, as_file, col_header, versions)
			puts "File #{as_file} successfully written."
		end
		return
	end

	# Ritorna un vettore contenente al primo posto un hash contenente per ogni categoria di smell
	# e al secondo posto un hash contenente gli header per ciascun AS suddivisi per categoria
	# la relativa matrice binaria di presenza dello smell o meno all'interno di una versione.
	# Le righe rappresentano le versioni, le colonne gli AS.
	# Esempio: matrix[:hublike] -> [[0, 1], [1, 1]]
	def binary_AS_matrices
		versions = Generic.get_all_versions @apache_dir
		uniq_as = unique_AS_list()
		
		h_headers = uniq_as[:hublike].map { |e| e.arc_smell_uuid }
		c_headers = uniq_as[:cyclic].map { |e| e.arc_smell_uuid }
		u_headers = uniq_as[:unstable].map { |e| e.arc_smell_uuid }

		h_matrix = []
		c_matrix = [] 
		u_matrix = []
		versions.each do |v|
			h_line = []
			c_line = []
			u_line = []
			uniq_as[:hublike].each_with_index do |h, i|
				raise "Hublike header mismatch" unless uniq_as[:hublike][i].arc_smell_uuid == h_headers[i]
				h_line << (@as_hash[v][:hublike].include?(h) ? 1 : 0);
			end
			h_matrix << h_line

			uniq_as[:cyclic].each_with_index do |c, i|
				raise "Cyclic header mismatch" unless uniq_as[:cyclic][i].arc_smell_uuid == c_headers[i]
				c_line << (@as_hash[v][:cyclic].include?(c) ? 1 : 0);
			end
			c_matrix << c_line

			uniq_as[:unstable].each_with_index do |u, i|
				raise "Unstable header mismatch" unless uniq_as[:unstable][i].arc_smell_uuid == u_headers[i]
				u_line << (@as_hash[v][:unstable].include?(u) ? 1 : 0);
			end
			u_matrix << u_line
		end
		matrices = {:hublike => h_matrix, :cyclic => c_matrix, :unstable => u_matrix }
		headers = {:hublike => h_headers, :cyclic => c_headers, :unstable => u_headers }
		return matrices, headers
	end

	# Scrive una matrice binaria su file in formato csv, con gli header indicati.
	def save_binary_matrix_to_csv(binary_matrix, file, col_header = [], row_header = [])
		CSV.open(file, "wb") do |csv|
			csv << col_header.unshift("Version") if !col_header.empty?
			binary_matrix.each_with_index do |e, i|
				row = []
				row << row_header[i] if !row_header.empty?
				row << e
				csv << row.flatten
			end
		end
	end

	# Ritorna un hash contenente tutti gli AS unici.
	# uniq_as_hash[:hublike] -> Lista di tutti gli hublike unici
	def unique_AS_list
		hublike = [] 	# Unique hublike list
		cyclic = [] 	# Unique cyclic list
		unstable = [] 	# Unique unstable list
		keys = Generic.get_all_versions @apache_dir
		as_hash.each do |k, v|
			hublike 	|= v[:hublike]
			cyclic 		|= v[:cyclic]
			unstable 	|= v[:unstable]
		end
		return { :hublike => hublike, :cyclic => cyclic, :unstable => unstable}
	end

	# Ritorna gli UUID degli elementi nella lista di AS fornita in input
	def unique_AS_uuid(as_list)
		h_headers = as_list[:hublike].map { |e| e.arc_smell_uuid }
		c_headers = as_list[:cyclic].map { |e| e.arc_smell_uuid }
		u_headers = as_list[:unstable].map { |e| e.arc_smell_uuid }
		headers = {:hublike => h_headers, :cyclic => c_headers, :unstable => u_headers }
	end

	# Dettagli AS univoci, 
	# - Lista delle versioni in cui appare
	# - Numero di volte in cui appare (ridondante rispetto alla prima)
	def unique_AS_details out_file = nil
		# potrebbe essere utile da implementare
	end

	# Ritorna un hash contenente per ogni categoria di smell un hash con chiave lo smell e valore una lista di versioni affette.
	# Ad esempio as_with_versions[:cyclic][a_smell] -> [6.0.1, 6.0.45]
	def unique_as_with_versions
		# fa la stessa cosa della matrice di AS
	end

	# Conteggio del numero di AS presenti in presenza di ogni singola CVE.
	# Ritorna un hash con due chiavi :affected e :not_affected.
	# Esempio cve_AS_count[:affected][cve1][:hublike] -> n° di hublike presenti quando cve1 è presente
	# Esempio cve_AS_count[:not_affected][cve1][:hublike] -> n° di hublike presenti quando cve1 NON è presente
	# Params:
	# +normalize+:: true se si desidera normalizzare il conteggio degli AS in base al numero massimo 
	# di ciascun tipo di AS, false altrimenti (default).
	def count_AS_per_CVE(normalize = false)
		total_as = total_AS_across_versions(false)
		norm_h = (normalize ? total_as[:hublike] : 1).to_f 	# to float
		norm_c = (normalize ? total_as[:cyclic] : 1).to_f
		norm_u = (normalize ? total_as[:unstable] : 1).to_f
		
		# Per ogni CVE calcolo, nelle versioni in cui è presente, quanti AS ci sono
		versions = Generic.get_all_versions @apache_dir
		count_AS = Hash.new							# Hash contenente per ogni CVE il numero di AS in cui la cve è presente
		ncount_AS = Hash.new 						# Hash contenente per ogni CVE il numero di AS in cui la cve NON è presente
		@cve_list.each do |cve|
			h_count = c_count = u_count = 0			# Counter AS in cui la cve è presente
			nh_count = nc_count = nu_count = 0		# Counter AS in cui la cve NON è presente
			versions.each do |v|
				if cve.affects?(v)
					h_count += (@as_hash[v][:hublike].uniq.count / norm_h)
					c_count += (@as_hash[v][:cyclic].uniq.count / norm_c)
					u_count += (@as_hash[v][:unstable].uniq.count / norm_u)
				else 
					nh_count += (@as_hash[v][:hublike].uniq.count / norm_h)
					nc_count += (@as_hash[v][:cyclic].uniq.count / norm_c)
					nu_count += (@as_hash[v][:unstable].uniq.count / norm_u)
				end
			end
			count = {:hublike => h_count, :cyclic => c_count, :unstable => u_count} # Numero di AS in cui la CVE è presente
			ncount = {:hublike => nh_count, :cyclic => nc_count, :unstable => nu_count} # Numero di AS in cui la CVE NON è presente
			count_AS[cve] = count
			ncount_AS[cve] = ncount
		end
		{:affected => count_AS, :not_affected => ncount_AS}
	end

	# Salva il numero di AS in presenza ed in assenza di ciascuna CVE su file.
	def count_AS_per_CVE_to_csv(out_dir, normalize = false)
		as_per_cve = count_AS_per_CVE(normalize)
		
		hl_file = "#{out_dir}/hublike_count_as_per_cve.csv"
		cd_file = "#{out_dir}/cyclic_count_as_per_cve.csv"
		un_file = "#{out_dir}/unstable_count_as_per_cve.csv"
		header = ["Cve ID", "#AS in affected versions", "#AS in NOT affected versions"]
		[hl_file, cd_file, un_file].each do |f|
			File.open(f, 'wb') {|file| 
				file.truncate(0) 
				file.write header.join(",")
				file.write "\n"
			}
		end


		@cve_list.each do |cve|
			aff_as_count = as_per_cve[:affected][cve]
			naf_as_count = as_per_cve[:not_affected][cve]
			CSV.open(hl_file, "ab") do |csv|
				csv << [cve.cve_id, aff_as_count[:hublike], naf_as_count[:hublike]]
			end
			CSV.open(cd_file, "ab") do |csv|
				csv << [cve.cve_id, aff_as_count[:cyclic], naf_as_count[:cyclic]]
			end
			CSV.open(un_file, "ab") do |csv|
				csv << [cve.cve_id, aff_as_count[:unstable], naf_as_count[:unstable]]
			end
		end
		puts "Files written successfully to #{out_dir}."
	end

	# Conta il numero total di smell rilevati da Arcan in tutte le versioni.
	# Ritorna un hash con un counter per ciascun tipo di smell
	# Esempio as_count[:hublike] -> 120, ovvero ci sono 120 :hublike in tutte le versioni
	# Se aggregate = true verrà ritornato un intero rappresentate il numero totale di smell.
	def total_AS_across_versions(aggregate = false)
		h_count = c_count = u_count = 0
		@as_hash.keys.each do |v|
			h_count += @as_hash[v][:hublike].count
			c_count += @as_hash[v][:cyclic].count
			u_count += @as_hash[v][:unstable].count
		end
		return {:hublike => h_count, :cyclic => c_count, :unstable => u_count} if !aggregate
		return h_count + c_count + u_count if aggregate;
	end

	# Conta il numero di AS rilevati da arcan in ciascuna versione e suddiviso per categoria di smell.
	# Se aggregate = true per ogni versione verrà ritornato un intero rappresentate il numero totale di smell.
	def total_AS_per_versions(aggregate = false)
		count = Hash.new
		@as_hash.each do |k, v|
			if !aggregate 
			count[k] = {:hublike  => v[:hublike].count,
						:cyclic   => v[:cyclic].count,
						:unstable => v[:unstable].count}
			else
				count[k] = v[:hublike].count + v[:cyclic].count + v[:unstable].count
			end
		end
		count
	end

	# Calcola la differenza di cve/as rispetto alla versione precedente.
	# Ritorna un hash con le seguenti chiavi: :cve, :hublike, :cyclic, :unstable.
	# Ogni hash contiene a sua volta un hash le cui chiavi sono le versioni e i valori la variazione nel numero
	# di cve/as rispetto alla versione precedente.
	# Esempio: delta[:cve] -> {"6.0.1"=> 63, "6.0.2"=>0 ...}
	def delta_CVE_AS_per_versions
		cve_delta = Hash.new
		hl_delta = Hash.new
		cd_delta = Hash.new
		un_delta = Hash.new

		versions = Generic.get_all_versions @apache_dir
		cve_delta[versions.first] = @cve_hash[versions.first].count
		hl_delta[versions.first] = @as_hash[versions.first][:hublike].count
		cd_delta[versions.first] = @as_hash[versions.first][:cyclic].count
		un_delta[versions.first] = @as_hash[versions.first][:unstable].count

		versions.each_with_index do |v, i|
			cve_delta[v] = @cve_hash[versions[i]].count - @cve_hash[versions[i-1]].count unless i - 1 < 0
			hl_delta[v] = @as_hash[versions[i]][:hublike].count - @as_hash[versions[i-1]][:hublike].count unless i - 1 < 0 
			cd_delta[v] = @as_hash[versions[i]][:cyclic].count - @as_hash[versions[i-1]][:cyclic].count unless i - 1 < 0 
			un_delta[v] = @as_hash[versions[i]][:unstable].count - @as_hash[versions[i-1]][:unstable].count unless i - 1 < 0 
		end
		{:cve=>cve_delta, :hublike=>hl_delta, :cyclic=>cd_delta, :unstable=>un_delta}
	end

	# Scrive su file i delta delle cve e degli as. Se split_by_major_version = true -> scrive 4 file
	def delta_CVE_AS_per_versions_to_csv (out_dir, split_by_major_version = false)
		delta = delta_CVE_AS_per_versions
		versions = Generic.get_all_versions @apache_dir

		six, seven = versions.partition{|x| CVE.compare_version(x, "7.0.0") < 0}
		seven, eight = seven.partition{|x| CVE.compare_version(x, "8.0.0") < 0}
		eight, eight2 = eight.partition{|x| CVE.compare_version(x, "8.5.0") < 0}
		eight2, nine = eight2.partition{|x| CVE.compare_version(x, "9.0.0.M1") < 0}
		m_versions = [six, seven, eight, eight2, nine] 
		
		m_versions = [m_versions.flatten] if !split_by_major_version

		m_versions.each do |sub_versions|
			header = "Version", "CVE", "Hublike", "Cyclic", "Unstable"
			file_name = out_dir + "delta" + sub_versions.first.split(".")[0..2].join("") + ".csv"
			file_name = out_dir + "delta.csv" if m_versions.size == 1
			CSV.open(file_name, "wb", :write_headers => true, :headers => header) do |csv|
				sub_versions.each do |v|
					csv << [v, delta[:cve][v], delta[:hublike][v], delta[:cyclic][v], delta[:unstable][v]]
				end
			end
		end
		puts "File #{out_dir} written successfully."
	end

	# Genera le tabelle di contingenza tetracoriche tra le categorie di smell HL/UD/CD ed ogni singola CVE
	# Il risultato è contenuto in un hash avente le cve come chiavi e come valori un hash per ciascuna delle
	# categorie di smell, ognuna contenente una matrice 2x2
	def contingency_CVE_AS(hl_threshold = 3, cd_threshold = 7, ud_threshold = 3, normalize = false)
		versions = Generic.get_all_versions @apache_dir
		bin_AS_mats = binary_AS_matrices[0]
		contingency = Hash.new

		empty_cm = [[0, 0],			# Righe CVE sì/no
					[0, 0]]			# Colonne AS sì/no

		@cve_list.each do |cve|
			contingency[cve.cve_id] = Hash.new { |hash, key| hash[key] = empty_cm.collect{|r| r.clone}}
			versions.each_with_index do |v, i|

				hl_tot = bin_AS_mats[:hublike][i].sum 				# Conta il numero di :hublike nell'i-esima versione
				hl_tot = hl_tot.to_f / bin_AS_mats[:hublike][i].size if normalize
				k = (cve.affects?(v) ? 0 : 1)						# Seleziona l'indice in cui contare +1 (righe)
				j = (hl_tot >= hl_threshold ? 0 : 1)    			# Seleziona l'indice su cui contare +1 (colonne)
				contingency[cve.cve_id][:hublike][k][j] += 1

				cd_tot = bin_AS_mats[:cyclic][i].sum
				cd_tot = cd_tot.to_f / bin_AS_mats[:cyclic][i].size if normalize
				j = (cd_tot >= cd_threshold ? 0 : 1)
				contingency[cve.cve_id][:cyclic][k][j] += 1

				ud_tot = bin_AS_mats[:unstable][i].sum
				ud_tot = ud_tot.to_f / bin_AS_mats[:unstable][i].size if normalize
				j = (ud_tot >= ud_threshold ? 0 : 1)
				contingency[cve.cve_id][:unstable][k][j] += 1
			end
		end
		contingency
	end

	# Scrive la matrice di contingenza relativa alle categorie di smell
	def contingency_CVE_AS_to_csv out_file, thresolds = [3,7,3]
		contingency = contingency_CVE_AS *thresolds
		CSV.open(out_file, "wb") do |csv|
			csv << ["CVE-ID", "HublikeMatrix", "CyclicMatrix", "UnstableMatrix"]
			contingency.each do |cve_id, cm|
				hl = cm[:hublike].collect{|r| r.join(",")}.join("#")
				cd = cm[:cyclic].collect{|r| r.join(",")}.join("#")
				ud = cm[:unstable].collect{|r| r.join(",")}.join("#")
				csv << [cve_id, hl, cd, ud]
			end
		end
		puts "File #{out_file} written successfully."
	end

	# Genera le matrici di contingenza tetracoriche tra ogni singolo smell e ogni singola cve
	# Ritorna un hash accessibile nel seguente modo:
	# contingency[cve_id][arc_smell_uuid] = [[x, y], [x', y'] -> che rappresenta la matrice di contingenza
	# dove le righe sono la presenza e no della cve, e le colonne la presenza o no dell'AS
	def contingency_CVE_single_AS
		versions = Generic.get_all_versions @apache_dir
		uniq_AS = unique_AS_list

		contingency = Hash.new
		@cve_list.each do |cve|
			contingency[cve.cve_id] = Hash.new
			uniq_AS.each do |type, smells|
				smells.each do |as|
					contingency[cve.cve_id][as.arc_smell_uuid] = [[0, 0], [0, 0]] 
					versions.each do |v|
						i = cve.affects?(v) ? 0 : 1
						j = @as_hash[v][type].include?(as) ? 0 : 1
						contingency[cve.cve_id][as.arc_smell_uuid][i][j] += 1
					end
				end
			end
		end
		contingency
	end

	# Scrive la matrice di contingenza CVE e AS singoli sul file csv indicato.
	def contingency_CVE_single_AS_to_csv out_file
		c_matrix = contingency_CVE_single_AS
		uuid = unique_AS_uuid(unique_AS_list)		# La lista garantisce che la colonna di ogni riga venga prelevata nel medesimo orgine
		uuid = uuid.values.flatten.sort
		CSV.open(out_file, "wb") do |csv|
			csv << ["CVE-ID", *uuid]
			c_matrix.each do |cve_id, matrices|
				row = [cve_id]
				unless matrices.keys.sort == uuid 
					puts "Matrices:#{matrices.keys.sort}"
					puts "Uuid:#{uuid}"
					raise "Collumns differ"
				end
				uuid.each do |smell_uuid|
					m = c_matrix[cve_id][smell_uuid].collect{|r| r.join(",")}.join("#")
					row << m
				end
				csv << row
			end
		end
		puts "File #{out_file} written successfully."
	end

	# Scrive nella cartella indicata tutti i risultati calcolati
	# e genera i dataset per l'analisi di correlazione tra CVE e AS.
	def generate_all_csv_data out_dir

		Dir.mkdir(out_dir) unless File.exists?(out_dir)

		success = lambda { |file| puts "Results successfully written on #{file}\n\n" }

		file = "#{out_dir}/AS_count_per_package.csv"
		puts "Counting AS per package..."
		AS.save_smells_per_package(@apache_dir, file)
		success[file]

		file = "#{out_dir}/CVE_count_per_version.csv"
		puts "Counting CVE per each version..."
		CVE.save_count_cve_for_each_version(@apache_dir, file)
		success[file]

		file = "#{out_dir}/CVE_versions_afflicted.csv"
		puts "Calculating afflicted versions for each CVE..."
		CVE.save_versions_cve_list(@apache_dir, file)
		success[file]

		file = "#{out_dir}/DATASET1"
		Dir.mkdir(file) unless File.exists?(file)
		puts "Counting AS in presence/assence of each CVE..."
		self.count_AS_per_CVE_to_csv(file, false)
		success[file]
		file = "#{out_dir}/DATASET1_norm"
		Dir.mkdir(file) unless File.exists?(file)
		puts "Calculating normalized version..."
		self.count_AS_per_CVE_to_csv(file, true)
		success[file]

		file = "#{out_dir}/DATASET2/"
		Dir.mkdir(file) unless File.exists?(file)
		"Calculating variation of CVE and AS on each version."
		self.delta_CVE_AS_per_versions_to_csv(file, false)
		self.delta_CVE_AS_per_versions_to_csv(file, true)
		success[file]

		file = "#{out_dir}/DATASET3/"
		Dir.mkdir(file) unless File.exists?(file)
		file = "#{file}contingency_CVE_AS.csv"
		puts "Calculating contingency matrix between CVE and AS categories..."
		self.contingency_CVE_AS_to_csv(file)
		success[file]

		file = "#{out_dir}/DATASET4/"
		Dir.mkdir(file) unless File.exists?(file)
		file = "#{file}contingency_CVE_single_AS.csv"
		puts "Calculating contingency matrix between CVE and each AS..."
		self.contingency_CVE_single_AS_to_csv(file)
		success[file]

		file = "#{out_dir}/DATASET5"
		Dir.mkdir(file) unless File.exists?(file)
		puts "Calculating binary matrices for AS and CVE..."
		self.binary_matrices_to_csv(file)
		success[file]
	end
end

class Array
	# Ritorna l'n-esima riga di un array 2D
	def get_row(n)
		self.collect{|x| x[n]}
	end

	# Calcola l'indice di Jaccard, ovvero l'indice si similarità di self e dell'array in input
	# L'indice è definito come J = (a & b).size / (a | b).size 
	def jaccard_index(array)
		intersection 	= self & array
		union 			= self | array
		j_index 		= intersection.size.to_f / union.size.to_f
	end

	def sum
		sum = 0
		self.each {|x| sum += x}
		sum
	end
end