varsdf <- as.data.frame(read.csv("../data_final/DATASET2/delta.csv", row.names = 1))

varsdf6 <- as.data.frame(read.csv("../data_final/DATASET2/delta600.csv", row.names = 1))
varsdf7 <- as.data.frame(read.csv("../data_final/DATASET2/delta706.csv", row.names = 1))
varsdf8 <- as.data.frame(read.csv("../data_final/DATASET2/delta801.csv", row.names = 1))
varsdf85 <- as.data.frame(read.csv("../data_final/DATASET2/delta850.csv", row.names = 1))
varsdf9 <- as.data.frame(read.csv("../data_final/DATASET2/delta900.csv", row.names = 1))

# All versions
wilcox.test(x=varsdf$CVE,y=varsdf$Hublike, paired=TRUE)
wilcox.test(x=varsdf$CVE,y=varsdf$Unstable, paired=TRUE)
wilcox.test(x=varsdf$CVE,y=varsdf$Cyclic, paired=TRUE)

# Sono sensibili all'eccessivo numero di valori identici:
# 	i test non sono affidabili al 100%
# Versione 6
wilcox.test(x=varsdf6$CVE,y=varsdf6$Hublike, paired=TRUE)
wilcox.test(x=varsdf6$CVE,y=varsdf6$Unstable, paired=TRUE)
wilcox.test(x=varsdf6$CVE,y=varsdf6$Cyclic, paired=TRUE)
# Versione 7
wilcox.test(x=varsdf7$CVE,y=varsdf7$Hublike, paired=TRUE)
wilcox.test(x=varsdf7$CVE,y=varsdf7$Unstable, paired=TRUE)
wilcox.test(x=varsdf7$CVE,y=varsdf7$Cyclic, paired=TRUE)
# Versione 8
wilcox.test(x=varsdf8$CVE,y=varsdf8$Hublike, paired=TRUE)
wilcox.test(x=varsdf8$CVE,y=varsdf8$Unstable, paired=TRUE)
wilcox.test(x=varsdf8$CVE,y=varsdf8$Cyclic, paired=TRUE)
# Versione 8.5
wilcox.test(x=varsdf85$CVE,y=varsdf85$Hublike, paired=TRUE)
wilcox.test(x=varsdf85$CVE,y=varsdf85$Unstable, paired=TRUE)
wilcox.test(x=varsdf85$CVE,y=varsdf85$Cyclic, paired=TRUE)
# Versione 9
wilcox.test(x=varsdf9$CVE,y=varsdf9$Hublike, paired=TRUE)
wilcox.test(x=varsdf9$CVE,y=varsdf9$Unstable, paired=TRUE)
wilcox.test(x=varsdf9$CVE,y=varsdf9$Cyclic, paired=TRUE)