hldf <- as.data.frame(read.csv("../data_final/DATASET1/hublike_count_as_per_cve.csv", row.names = 1))
cddf <- as.data.frame(read.csv("../data_final/DATASET1/cyclic_count_as_per_cve.csv", row.names = 1))
uddf <- as.data.frame(read.csv("../data_final/DATASET1/unstable_count_as_per_cve.csv", row.names = 1))

# H0 : Location shift (Median) is equal to 0 (aka non � cambiato nulla)
# H1 : Location shift (Median) is not equal to 0 (aka qualcosa � cambiato)

wilcox.test(x=hldf$X.AS.in.affected.versions,y=hldf$X.AS.in.NOT.affected.versions, paired = TRUE)

wilcox.test(x=cddf$X.AS.in.affected.versions,y=cddf$X.AS.in.NOT.affected.versions, paired = TRUE)

wilcox.test(x=uddf$X.AS.in.affected.versions,y=uddf$X.AS.in.NOT.affected.versions, paired = TRUE)