#setwd("/home/fenn/Documenti/git/progetto-reverse/analysis/")
source("mylib.r")
rawdata <- read.csv("../data_final/DATASET3/contingency_CVE_AS.csv", row.names = 1)

errorCount = matrix(nrow=1,ncol=3, dimnames = list(NULL,colnames(rawdata)))
pvalues <- matrix(nrow = nrow(rawdata), ncol = ncol(rawdata), 
									dimnames = list(rownames(rawdata),colnames(rawdata)))

for(i in 1:nrow(rawdata)){
  for(j in 1:ncol(rawdata)){
    contingency = parse.contingency.matrix(as.character(rawdata[[i,j]]))
    res <- tryCatch({
    	res = chisq.test(contingency)$p.value # res$statistic
    }, warning = function(w){
    	return(-1) # Imposta res a -1
    }, error = function(e){
    	return(-1)
    })
    pvalues[[i, j]] = res
  }
}

ds3 <- as.data.frame(pvalues)
# Plots
plot(pvalues[,1],col=ifelse(pvalues[,1]<0.05,ifelse(pvalues[,1]>= 0, "blue", "red"), "green"), xlab = "CVE Index", ylab = "Hublike", xlim=c(0,120))
plot(pvalues[,2],col=ifelse(pvalues[,2]<0.05,ifelse(pvalues[,2]>= 0, "blue", "red"), "green"), xlab = "CVE Index", ylab = "Cyclic", xlim=c(0,120))
plot(pvalues[,3],col=ifelse(pvalues[,3]<0.05,ifelse(pvalues[,3]>= 0, "blue", "red"), "green"), xlab = "CVE Index", ylab = "Unstable", xlim=c(0,120))
legend(85,0.9, c("p > 0.05", "0 < p <= 0.05", "p = -1"), lwd=c(1,1,1),col=c("green", "blue","red"),  lty=c(0,0,0), pch=c(1,1,1))
