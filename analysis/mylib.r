parse.contingency.matrix <- function(tablestr, rsep="#", csep=","){
  rows <- unlist(strsplit(tablestr, rsep))
  matrix = NULL
  for(row in rows){
    row_array <- as.integer(unlist(strsplit(row, csep)))
    if(is.null(matrix)){
      matrix <- rbind(row_array)
    }else{
      matrix <- rbind(matrix, row_array)
    }
  }
  return(matrix)
}