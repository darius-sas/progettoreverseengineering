cvedf <- as.data.frame(read.csv("../data_final/DATASET5/cve_matrix.csv", row.names = 1))
hldf <- as.data.frame(read.csv("../data_final/DATASET5/hublike_matrix.csv", row.names = 1))
cddf <- as.data.frame(read.csv("../data_final/DATASET5/cyclic_matrix.csv", row.names = 1))
uddf <- as.data.frame(read.csv("../data_final/DATASET5/unstable_matrix.csv", row.names = 1))

# Chi-Square (test of indipendence)
# H0 : There is no difference between the distributions
# H1 : There is difference between distributions

# For typical analysis, using the standard alpha = 0.05 cutoff, 
# the null hypothesis is rejected when p < .05 and not rejected when p > .05. 

analizer = function(cve, as){
	results = matrix(nrow = ncol(cve), ncol = ncol(as), dimnames = list(colnames(cve), colnames(as)))
	for(cve_col in 1:ncol(cve))
		for(as_col in 1:ncol(as)){
			res <- tryCatch({
				tab = table(cve[,cve_col], as[,as_col])
				if(ncol(tab) < 2 || nrow(tab) < 2){
					res = -1
				}else{
					res = chisq.test(tab)$p.value
				}
			}, warning = function(w){
				return(-1)
			}, error = function(e){
				return(-1)
			})
			results[[cve_col, as_col]] = res
		}
	return(results)
}

hlpvalues = analizer(cvedf, hldf)
cdpvalues = analizer(cvedf, cddf)
udpvalues = analizer(cvedf, uddf)

# Plotting
getcolor = function(column){
		return(ifelse(column <= 0.05, ifelse(column >= 0, "blue", "red"), "green"))
}

plot.pvalues = function(pvaluesmatrix, ylab = "Architectural Smell", xlab = "CVE Index"){
	plot(pvaluesmatrix[,1], col= getcolor(pvaluesmatrix[,1]), xlim = c(0, 120), ylim = c(-1, 1), xlab = xlab, ylab = ylab)
	for(i in 1:ncol(pvaluesmatrix)){
		points(pvaluesmatrix[,i], col = getcolor(pvaluesmatrix[,i]))	
	}
	legend(85,0.90, c("p > 0.05", "0 < p <= 0.05", "p = -1"), lwd=c(1,1,1),col=c("green", "blue","red"),  lty=c(0,0,0), pch=c(1,1,1))
}

plot.pvalues(hlpvalues, ylab = "Hublike")
plot.pvalues(cdpvalues, ylab = "Cyclic")
plot.pvalues(udpvalues, ylab = "Unstable")
