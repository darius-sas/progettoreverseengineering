#!/bin/ruby
require 'nokogiri' # parse html gem
require 'open-uri' # nokogiri module
require 'csv'

class String
    def is_i?
       !!(self =~ /\A[-+]?[0-9]+\z/)
    end
end

module CVE

	APACHE_MAJOR_VERSIONS = [6, 7, 8, 9]

	class CveTomcatEntry
		attr_accessor :cve_id, :cve_severity, :cve_description
		attr_reader :cve_affects
		
		NOT_AVAILABLE = "ND"

		def initialize
			@cve_id = NOT_AVAILABLE
			@cve_severity = NOT_AVAILABLE
			@cve_description = NOT_AVAILABLE
			@cve_affects = []
			@cve_affects << CveVersionSpan.new(NOT_AVAILABLE, NOT_AVAILABLE)
		end

		def add_cve_affect(startv, endv)
			@cve_affects = [] if @cve_affects[0].start_ver == NOT_AVAILABLE
			@cve_affects << CveVersionSpan.new(startv, endv)
		end

		# Se i due oggetti CveTomcatEntry rappresentano la stessa CVE,
		# aggiunge le versioni a cve_affects dell'oggetto corrente
		def add_cve_affect_from_cve(other_cve)
			return if other_cve.cve_id != @cve_id
			other_cve.cve_affects.each do |version_span|
				next if @cve_affects.include? version_span
				add_cve_affect(version_span.start_ver, version_span.end_ver)
			end
		end

		def remove_cve_affect(start_ver, end_ver)
			@cve_of_versions.delete_if {|e| e.start_ver == start_ver && e.end_ver == end_ver}
		end

		# Vero se la versione fornita in input è affetta, falso altrimenti.
		def affects?(version)
			affects = false
			@cve_affects.each do |cve_span|
				affects = (affects or cve_span.affects?(version))
			end
			affects
		end

		# Controlla se l'istanza corrente contiene un intervallo di versioni affette
		def versions_available?
			return !(@cve_affects.empty? || @cve_affects.include?(CveVersionSpan.new(NOT_AVAILABLE, NOT_AVAILABLE)))
		end

		def to_s
			format("ID: %-16s Severity: %-10s Description: %s\tAffects: %s", @cve_id, @cve_severity, @cve_description, @cve_affects.join(", "))
		end

		# Due CVE sono identiche se hanno lo stesso id
		def ==(other)
			return false if other == nil
			return other.cve_id == @cve_id
		end

		# Delega a == il compare
		def eql?(other)
			other == self
		end

		# L'hash corrisponde all'hash della stringa di @cve_id
		def hash
			@cve_id.hash
		end
	end

	class CveVersionSpan
		attr_accessor :start_ver, :end_ver

		def initialize(start_ver, end_ver)
			@start_ver, @end_ver = start_ver, end_ver
		end

		def to_s
			format("%s-%s", @start_ver, @end_ver)
		end

		def ==(other)
			return false if other == nil
			return compare_version(@start_ver, other.start_ver) == 0 && compare_version(@end_ver, other.end_ver) == 0 
		end

		# Controlla se la versione fornita in input è compresa nell'intervallo dell'istanza
		def affects?(version)
			return compare_version(@start_ver, version) <= 0 && compare_version(@end_ver, version) >= 0
		end

		# Compara due versioni
		# Se v1 > v2 -> 1
		# Se v1 == v2 -> 0
		# Se v1 < v2 -> -1
		def compare_version(v1, v2)
			CVE.compare_version(v1, v2)
		end
	end

	# Parsa dalla pagina ufficiale di Tomcat le vulnerabilità presenti nelle varie versioni. Ritorna una lista di CveTomcatEntry
	# ATTENZIONE: le CVE non sono univoche, ciò vuol dire che vanno unificate adeguatamente in caso
	# 			  la stessa CVE è presente in più range di versioni. Usare CVE.get_cve_list.
	# Params:
	# +apache_major_version+:: Major version di Tomcat di cui estrarre le vulnerability. Testato su Tomcat 6-7-8-9.
	def CVE.extract_cve(apache_major_version)
		cve_page = Nokogiri::HTML(open("http://tomcat.apache.org/security-#{apache_major_version}.html"))
		cve_list = []
		cve_page.css("div.text").each do |div_tag|
			p_tags = div_tag.css("p");
			for i in 0...p_tags.size
				p_tag = p_tags[i]
				next if p_tag.name != "p" 
				cve_html_line = p_tag.text
				
				# Match CVE p tags
				regexp_cve = /((Low|Moderate|Important|Critical):[\w\s-]+CVE-\d+-\d+)/
				if cve_html_line =~ regexp_cve
					cve_entry = CveTomcatEntry.new
					cve_entry.cve_id = cve_html_line[/CVE-\d+-\d+/].strip
					cve_entry.cve_severity = cve_html_line[/(Low|Moderate|Important|Critical)/].strip
					cve_entry.cve_description = cve_html_line[/:(.*?)CVE/m,1].strip
					for j in (i+1)...p_tags.size
						cve_html_line = p_tags[j].text
						break if cve_html_line =~ regexp_cve
						#Look for CVE affects p tags
						if cve_html_line =~ /(Affects:\s{0,3}\d+\.\d+\.\d+(\.(RC)?M?\d+)?\s{0,3}((-|to)\s{0,3}\d+\.\d+\.\d+(\.(RC)?M?\d+)?)?)/
							cve_html_line.slice!(/Affects:\s{,3}/)
							cve_html_line.split(",").each do | e |
								e.sub!("-RC", ".RC")
								startv, sep, endv = e.split(/(to|-)/)
								endv = startv if endv.nil?
								cve_entry.add_cve_affect(startv.strip, endv.strip)
							end
							break		
						end
					end
					cve_list << cve_entry
				end
			end
		end
		return cve_list			
	end

	# Ritorna un hash che ha come chiave un elemento di 'apache_version'
	# e come valori tutte le cve che affliggono una certa chiave k.
	# Params:
	# +apache_versions_dir+:: Cartella contenente le versioni di apache tomcat, ognuna in una cartella separata
	def CVE.get_versions_cve_list(apache_versions_dir)
		apache_versions = Generic.get_all_versions(apache_versions_dir)
		
		cve_list = get_cve_list

		cve_of_versions = Hash.new
		version_cve = []
		apache_versions.each do |v|
			cve_list.each do |cve|
				version_cve << cve if (cve.versions_available? && cve.affects?(v))
			end
			cve_of_versions[v] = version_cve
			version_cve = []
		end
		return cve_of_versions
	end

	# Ritorna una lista di tutte le CVE parsate 
	def CVE.get_cve_list
		cve_list = []
		APACHE_MAJOR_VERSIONS.each do |mv|
			version_cve_list = CVE.extract_cve(mv)
			version_cve_list.each do |cve|
				indx = cve_list.index cve
				if !indx.nil? # La cve esiste già, aggiungo le nuove versioni
					cve_list[indx].add_cve_affect_from_cve(cve)
				else
					cve_list << cve
				end
			end
		end
		cve_list.reject! do |cve|
			cve.cve_affects.first.start_ver == "ND"
		end
		cve_list
	end

	# +apache_versions_dir+:: Cartella contenente le versioni di apache tomcat, ognuna in una cartella separata
	# +out_file+:: File di output
	def CVE.save_count_cve_for_each_version(apache_versions_dir, out_file)
		cve_of_versions = get_versions_cve_list apache_versions_dir
		CSV.open(out_file, "wb") do |csv|
			csv << ["Version", "CVE count", "CVE List"]
			cve_of_versions.each do |v, cve|
				cve_ids = []
				cve.each {|c| cve_ids << c.cve_id}
				csv << [v, cve.size, *cve_ids]
			end
		end
		puts "File successfully written"
	end

	# Salva su file csv la lista delle CVE che affligge ogni versione
	# +apache_versions_dir+:: Cartella contenente le versioni di apache tomcat, ognuna in una cartella separata
	# +out_file+:: File di output
	def CVE.save_versions_cve_list(apache_versions_dir, out_file)
		cve_of_versions = get_versions_cve_list apache_versions_dir
		CSV.open(out_file, "wb") do |csv|
			csv << ["Version", "CVE", "Afflicted versions", "Severity", "Description"]
			cve_of_versions.each do |v, cve_list|
				cve_list.each do |cve|
					csv << [v, cve.cve_id, cve.cve_affects.join(", "), cve.cve_severity, cve.cve_description]
				end
			end
		end
		puts "File successfully written"
	end

	# Comparatore per versioni apache.
	# Se v1 > v2 -> 1
	# Se v1 == v2 -> 0
	# Se v1 < v2 -> -1
	def CVE.compare_version(v1, v2)
		v1_a = v1.split('.')
		v2_a = v2.split('.')
		min_length = [v1_a.size, v2_a.size].min
		result = 0
		for i in 0...min_length
			# Controlla se sono numeri, se non lo sono prendi solo la parte numerica
			v1_a[i] = v1_a[i][/\d+/] if !v1_a[i].is_i?
			v2_a[i] = v2_a[i][/\d+/] if !v2_a[i].is_i?

			next if v1_a[i].to_i == v2_a[i].to_i
			if v1_a[i].to_i > v2_a[i].to_i
				result = 1
				break
			else
				result = -1
				break
			end
		end
		return result
	end
end
