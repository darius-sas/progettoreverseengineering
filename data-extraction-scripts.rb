#!/usr/bin/ruby
require './common-vulnerabilities'
require 'parallel'

module Generic
	# Esegue arcan su tutte le versioni di tomcat presenti all'interno della cartella fornita in input.
	# Ritorna una hash con chiave la versione che ha causato l'errore e valore il valore di ritorno di arcan
	# THIS METHOD IS NOT SAFE
	# Params:
	# +apache_versions_dir+:: Cartella contenente le versioni di apache tomcat, ognuna in una cartella separata
	def Generic.run_arcan(apache_versions_dir)
		Dir.chdir(apache_versions_dir)

		apache_dirs = Dir.entries(".").select {|f| File.directory?(f)}
		apache_dirs.delete("..")
		apache_dirs.delete(".")
		apache_dirs.delete("zips")
		apache_dirs.delete("ToySystem-graph.graphml")

		error_mapping = Hash.new

		# Salta cartelle che contengono già i risultati
		apache_dirs = apache_dirs.select {|f| File.directory?(f) and (!Dir.entries(f).include?("results") or Dir.entries("#{f}/results").empty?)}
		#apache_dirs = ["apache-tomcat-6.0.0"]
		apache_dirs.sort
		Parallel.each_with_index(apache_dirs, in_processes: 4, 
											  start: -> (item, i) {puts "Running arcan on #{item[/\d+\.\d+\.\d+/]}\t#{i+1}/#{apache_dirs.size}"},
								) do |adir, indx|
			projectFolder = "#{adir}/lib"
			outputDir = "#{adir}/results"
			dbFolder = "#{adir}/arcan-db"
			arcan_command = "arcan -folderOfJars -p \"#{projectFolder}\" -d \"#{dbFolder}\" -out \"#{outputDir}\" -filter -neo4j -CM -HL -PM -UD -CD"
			apache_version = adir[/\d+\.\d+\.\d+/];
			#puts "#{arcan_command}"
			success = system(arcan_command, :out => File::NULL)
		end

=begin
		apache_dirs.each_with_index do |adir, indx| 
			projectFolder = "#{adir}/lib"
			outputDir = "#{adir}/results"
			dbFolder = "#{adir}/arcan-db"
			arcan_command = "arcan -folderOfJars -p \"#{projectFolder}\" -d \"#{dbFolder}\" -out \"#{outputDir}\" -filter -neo4j -CM -HL -PM -UD -CD"
			apache_version = adir[/\d+\.\d+\.\d+/];
			puts ""
			puts "*"*20
			puts ""
			puts "Running on version #{apache_version}. Progress #{indx+1}/#{apache_dirs.size}"
			puts "#{arcan_command}"
			success = system(arcan_command)
		end
=end
		return error_mapping
	end

	# Esegue il comando 'unzip' su tutti i file zip presenti nella cartella indicata in input. Rimuove tutti i file che non servono per l'analisi di arcan
	# THIS METHOD IS NOT SAFE
	# Params:
	# +apache_versions_dir+:: Cartella contenente le versioni di apache tomcat, ognuna in una cartella separata
	def Generic.unzip_and_remove_unused(apache_versions_dir)
		Dir.chdir(apache_versions_dir)
		zip_files = Dir.entries(".").select {|f| !File.directory? f}

		zip_files.each do |f| 
			system("unzip #{f}")
			system("mv #{f} zips")
		end

		apache_dirs = Dir.entries(".").select {|f| File.directory? f}
		apache_dirs.delete(".")
		apache_dirs.delete("..")
		apache_dirs.delete("zips")

		apache_dirs.each do |dir|
			system("mv #{dir}/bin/*.jar #{dir}/lib")
			dir_files = Dir.entries(dir).select {|f| f != "lib" and f != "results" and f != "arcan-db"}
			dir_files.delete(".")
			dir_files.delete("..")
			dir_files.each do |e| 
				system("rm -rf ./#{dir}/#{e}")
			end
		end
	end

	# Parsa un file contenete le versioni di apache tomcat in formato non definito (max 1 versione per riga) e scrive una lista di uri sul file di output
	# Il file può successivamente essere utilizzato con il comando "wget -nc -i 'output_file'"
	# Params:
	# +input_file_path+:: File in input contenete al massimo una versione per ogni riga
	# +output_file_path+:: File nel quale scrivere gli uri generati
	def Generic.create_download_uri_file(input_file_path, output_file_path)
		versions = []
		File.open(input_file_path).each do |line|
			version = line.scan(/(v\d+\.\d+\.\d+(\.M\d*)?)/).first.first
			versions << version if version != nil
		end

		versions = versions.uniq.sort

		uris = []
		versions.each do |version|
			major_version = version[1]
			version_num = version.sub("v", "")
			download_uri = "http://archive.apache.org/dist/tomcat/tomcat-#{major_version}/#{version}/bin/apache-tomcat-#{version_num}.zip"
			uris << download_uri
		end

		File.open(output_file_path, "w+") do |f|
			f.puts(uris)
		end
	end

	# Ritorna tutte le versioni di apache presenti nella cartella in input
	def Generic.get_all_versions(apache_versions_dir)
		versions = Dir.entries(apache_versions_dir).collect do |f|
			f[/(\d+\.\d+\.\d+(\.M\d*)?)/]
		end
		return versions.compact.uniq.sort {|x, y| CVE.compare_version(x, y)}
	end
end